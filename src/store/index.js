import { createStore } from 'vuex'

export default createStore({
  state: {
    filtersWindow: false,
    countryFilterWindow: false,
    sphereFilterWindow: false,
    sortWindow: false,
    isCountryDropdownVisible: false,
    isSphereDropdownVisible: false,
    countries: [
      { name: 'Algeria', checked: true },
      { name: 'Angola', checked: true },
      { name: 'Ascension', checked: true },
      { name: 'Benin', checked: true },
      { name: 'Botswana', checked: true },
      { name: 'Burkina Faso', checked: true },
      { name: 'Burundi', checked: true },
      { name: 'Cabo Verde', checked: true },
      { name: 'Cameroon', checked: true },
      { name: 'Central', checked: true },
      { name: 'African Republic', checked: true },
      { name: 'Ceuta', checked: true },
      { name: 'Chad', checked: true },
      { name: 'Comoros', checked: true },
      { name: 'Côte d’Ivoire', checked: true },
      { name: 'Djibouti', checked: true },
      { name: 'Egypt', checked: true },
      { name: 'Eritrea', checked: true },
      { name: 'Eswatini', checked: true },
      { name: 'Ethiopia', checked: true },
      { name: 'Gabon', checked: true },
      { name: 'Guinea', checked: true },
      { name: 'Guinea-Bissau', checked: true },
      { name: 'Kenya', checked: true },
      { name: 'Liberia', checked: true },
      { name: 'Madagascar', checked: true },
      { name: 'Malawi', checked: true },
      { name: 'Mali', checked: true },
      { name: 'Mauritania', checked: true },
      { name: 'Mauritius', checked: true },
      { name: 'Mayotte', checked: true },
      { name: 'Melilla', checked: true },
      { name: 'Mosambigue', checked: true },
      { name: 'Namibia', checked: true },
      { name: 'Niger', checked: true },
      { name: 'Nigeria', checked: true },
      { name: 'Réunion', checked: true },
      { name: 'Rwanda', checked: true },
      { name: 'Saint Helena', checked: true },
      { name: 'Senegal', checked: true },
      { name: 'Seychelles', checked: true },
      { name: 'Sierra Leone', checked: true },
      { name: 'Somalia', checked: true },
      { name: 'South Sudan', checked: true },
      { name: 'Sudan', checked: true },
      { name: 'Tanzania', checked: true },
      { name: 'Uganda', checked: true }
    ],
    spheres: [
      { name: 'Primary', checked: true },
      { name: 'Secondary', checked: true },
      { name: 'Tertiary', checked: true }
    ],
    latest: false,
    interest: true
  },
  getters: {
    GET_FILTERS_WINDOW(state){
      return state.filtersWindow
    },
    GET_COUNTRY_FILTER_WINDOW(state){
      return state.countryFilterWindow
    },
    GET_SPHERE_FILTER_WINDOW(state){
      return state.sphereFilterWindow
    },
    GET_SORT_WINDOW(state){
      return state.sortWindow
    },
    GET_COUNTRY_DROPDOWN(state){
      return state.isCountryDropdownVisible;
    },
    GET_SPHERE_DROPDOWN(state){
      return state.isSphereDropdownVisible;
    },
    GET_COUNTRY_LIST(state){
      return state.countries;
    },
    GET_SPHERE_LIST(state){
      return state.spheres;
    },
    GET_LATEST(state){
      return state.latest;
    },
    GET_INTEREST(state){
      return state.interest;
    }
  },

  mutations: {
    TOGGLE_FILTERS_WINDOW (state) {
      state.filtersWindow = !state.filtersWindow
    },
    TOGGLE_SORT_WINDOW (state) {
      state.sortWindow = !state.sortWindow
    },
    TOGGLE_COUNTRY_FILTER_WINDOW (state) {
      state.countryFilterWindow=!state.countryFilterWindow
    },
    TOGGLE_SPHERE_FILTER_WINDOW (state) {
      state.sphereFilterWindow=!state.sphereFilterWindow
    },
    TOGGLE_COUNTRY_DROPDOWN (state) {
      state.isCountryDropdownVisible = !state.isCountryDropdownVisible
    },
    TOGGLE_SPHERE_DROPDOWN (state) {
      state.isSphereDropdownVisible = !state.isSphereDropdownVisible
    },
    CHECK_COUNTRY (state,countryObj) {
      const c = state.countries.find(c=>c.name==countryObj.name);
      c.checked = !countryObj.checked;
    },
    CHECK_SPHERE (state,countryObj) {
      const c = state.spheres.find(c=>c.name==countryObj.name);
      c.checked = !countryObj.checked;
    },
    TOGGLE_LATEST(state){
      state.latest=!state.latest;
    },
    TOGGLE_INTEREST(state){
      state.interest=!state.interest;
    },
  },
  actions: {
    RUN_FILTERS_WINDOW(mutat){
      mutat.commit('TOGGLE_FILTERS_WINDOW');
    },
    RUN_COUNTRY_FILTER_WINDOW(mutat){
      mutat.commit('TOGGLE_COUNTRY_FILTER_WINDOW');
    }
  },
  modules: {
  }
})
